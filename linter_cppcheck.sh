linter_cpp() {
        file="$1"
        output=$(cppcheck --enable=all "$file" 2>&1)
        if [ -n "$output" ]; then
                echo "No linting issues in $file"
        else
                echo "Linting issues in $file:"
                echo "$output"
                exit 1
        fi
}

if [ $# -ne 1 ]; then
        echo "Usage: $0 <cpp_file>"
        exit 1
fi

cpp_file="$1"
linter_cpp "$cpp_file"

