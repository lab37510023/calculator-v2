#!/bin/bash

function function_test {
    local input=$1
    local output=$2

    local actual_output=$(echo ${input} | ./calculator.out)

    if [[ "${actual_output}" == "${output}" ]]; then
        echo "Test is passed"
    else
        echo "Test failed"
        echo "Expected: ${output}"
        echo "Got:      ${actual_output}"
        exit 1
    fi
}

function_test "2+3*4-16/2" "Enter the expression: The result is: 2"
